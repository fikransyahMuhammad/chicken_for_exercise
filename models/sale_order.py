from odoo import models, fields

class SaleOrder(models.Model):
    _inherit = "sale.order"

    reservation_stock = fields.Boolean()

    