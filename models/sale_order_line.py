from odoo import models, fields

class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"
    _description = "Create new field named quantity available stock"

    available_stock_qty = fields.Float(string="Available Stock", compute="_get_available_stock_qty", readonly=True)
    
    def _get_available_stock_qty(self):
        """
        get count of product on active warehouse
        """
        for order_line in self:
            # check whether the type of product is storable ?
            if order_line.product_id.detailed_type == "product":
                order_line.available_stock_qty = order_line.product_id.product_tmpl_id.qty_available
            else :
                order_line.available_stock_qty = 0

    def reserve_stock(self, res_id):
        """
        reserve stock action
        """
        order_lines = self.search([('order_id', '=', res_id)])
        for order_line in order_lines:
            new_virtual_availability = order_line.product_template_id.virtual_available - order_line.product_uom_qty

            order_line.product_template_id.virtual_available = new_virtual_availability
            
        return True

    def unreserve_stock(self, res_id):
        """
        unreserve stock action
        """
        order_lines = self.search([('order_id', '=', res_id)])
        for order_line in order_lines:
            new_virtual_availability = order_line.product_template_id.virtual_available + order_line.product_uom_qty

            order_line.product_template_id.virtual_available = new_virtual_availability
            
        return True