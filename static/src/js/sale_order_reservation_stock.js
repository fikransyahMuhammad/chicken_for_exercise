odoo.define('pitik_assignment.reservation_stock', function(require) {
    var AbstractField = require('web.AbstractField');
    var FieldRegistry = require('web.field_registry');

    var core = require('web.core');
    var qweb = core.qweb;

    var WidgetOne = AbstractField.extend({
        template: 'WidgetOneTemplate',
        events: {
            'click #btn_reservation_stock': '_reservation_stock_action'
        },
        _reservation_stock_action: function(e) {
            this._setValue(!this.value);
            var self = this;
            
            if (self.res_id == undefined) {
                alert("Save quotation first !");
            } else {
                if (!self.value) {
                    self._rpc({
                        model: self.attrs.relatedModel,
                        method: self.attrs.modifiers.relatedActionReserve,
                        args: [[], self.res_id]
                    }).then(function(result) {
                        console.log(result);
                    });
                } else {
                    self._rpc({
                        model: self.attrs.relatedModel,
                        method: self.attrs.modifiers.relatedActionUnreserve,
                        args: [[], self.res_id]
                    }).then(function(result) {
                        console.log(result);
                    });
                }
            }
        },
        get_title() {
            if (this.value == false) {
                return "Reservation Stock";
            } else {
                return "Cancel Reservation Stock";
            }
        },
        _render: function() {
            this.$el.html($(qweb.render(this.template, {'widget': this})));
        }
    });

    FieldRegistry.add('widget_one', WidgetOne);
    return WidgetOne;
});