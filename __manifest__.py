{
    'name': "Galih - Pitik Assignment",
    'summary': "First Assignment for Candidate ERP Developer",
    'description': """
        v0.1.0
        * create new field named available stock in order line
        * show available stock if product type is storable
        * create new field named reservation stock 
        * create button to reserve and unreserve virtual available
        By: Galih Fikran Syah
    """,

    'author': "Pitik Developer",
    'website': "http://www.pitik.id",
    'version': '15.0.1.0.0',
    'depends': ['sale', 'stock'],

    'data': [ 
        'views/sale_order_view.xml',
    ],
    'assets': {
        'web.assets_backend': [
            'pitik_assignment/static/src/js/sale_order_reservation_stock.js',
        ],
        'web.assets_qweb': [
            'pitik_assignment/static/src/xml/view.xml',
        ], 
    },
    'qweb': [ 'static/src/xml/view.xml' ],
    
    'auto_install': False,
    'installable': True,
    'application': False,
    'license': 'OEEL-1'
}
